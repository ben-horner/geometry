package bkh.matrix

import Jama.{Matrix => JM}

import scala.math.{max, min}
import scala.util.Random

object Matrix {

  def fromArrays(arrays: Array[Array[Double]]) = Matrix(arrays.map(_.toIndexedSeq).toIndexedSeq)
  def fromRows(rows: IndexedSeq[IndexedSeq[Double]]): Matrix = Matrix(rows)
  def fromCols(cols: IndexedSeq[IndexedSeq[Double]]): Matrix = Matrix(cols).transpose
  def fromRow(row: IndexedSeq[Double]): Matrix = Matrix.fromRows(IndexedSeq(row))
  def fromCol(col: IndexedSeq[Double]): Matrix = Matrix.fromCols(IndexedSeq(col))
  def fromPacked(packed: IndexedSeq[Double], numRows: Int): Matrix = Matrix.fromRows(packed.grouped(packed.size / numRows).toIndexedSeq)

  // accepts a function which maps the (row, col) pair to the value for the cell
  // all kinds of information can be captured in the function (such as two matrices being operated on)
  def fromIndexFunc(numRows: Int, numCols: Int, indexFunc: (Int, Int) => Double): Matrix = {
    Matrix(
      (0 until numRows).map{ row =>
        (0 until numCols).map{ col =>
          indexFunc(row, col)
        }
      }
    )
  }

  def zero(numRows: Int, numCols: Int): Matrix = Matrix.fromIndexFunc(numRows, numCols, (_, _) => 0.0)
  def constant(numRows: Int, numCols: Int, constant: Double): Matrix = Matrix.fromIndexFunc(numRows, numCols, (_, _) => constant)

  def identity(size: Int): Matrix = Matrix.fromIndexFunc(size, size, (r, c) => if (r == c) 1.0 else 0.0)
  def random(numRows: Int, numCols: Int, rng: Option[Random] = None): Matrix =
    Matrix.fromIndexFunc(numRows, numCols, (_, _) => rng.map(_.nextDouble()).getOrElse(Random.nextDouble()))

  def determinant(m: Matrix): Double = {
    require(m.numRows == m.numCols, s"determinant only defined for square matrices, not ${m.numRows}x${m.numCols}")
    require(m.numRows > 0, s"determinant not defined for degenerate matrices (${m.numRows}x${m.numCols})")
    if (m.numCols == 1) {
      m(0, 0)
    } else { // m.numCols > 1
      (for(col <- 0 until m.numCols) yield {
        val coeff = m(0, col)
        val skipMatrix = m.subMatrix((0 until col) ++ (col+1 until m.numCols), 1 until m.numRows)
        math.pow(-1, col) * coeff * determinant(skipMatrix)
      }).sum
    }
  }

  // TODO: should probably add requires to these decompositions to check internal properties (matrix sizes and such)
  case class EigenvalueDecomposition(V: Matrix, realEigenvalues: IndexedSeq[Double], imaginaryEigenvalues: IndexedSeq[Double], D: Matrix)

  case class SingularValueDecomposition(U: Matrix, V: Matrix, singularValues: IndexedSeq[Double], S: Matrix, norm2: Double, cond: Double, rank: Int)

  class CholeskyDecomposition(internal: Jama.CholeskyDecomposition) {

    val isSPD: Boolean = internal.isSPD

    val L: Matrix = Matrix.fromArrays(internal.getL.getArray)

    def solve(that: Matrix): Matrix = {
      Matrix.fromArrays(
        internal.solve(
          new JM(that.toArrays)
        ).getArray
      )
    }

  }

  class LUDecomposition(internal: Jama.LUDecomposition) {

    val isNonsingular: Boolean = internal.isNonsingular

    val L: Matrix = Matrix.fromArrays(internal.getL.getArray)

    val U: Matrix = Matrix.fromArrays(internal.getU.getArray)

    val pivot: IndexedSeq[Int] = internal.getPivot

    val doublePivot: IndexedSeq[Double] = internal.getDoublePivot

    val det: Double = internal.det()

    def solve(that: Matrix): Matrix = {
      Matrix.fromArrays(
        internal.solve(
          new JM(that.toArrays)
        ).getArray
      )
    }

  }

  class QRDecomposition(internal: Jama.QRDecomposition) {

    val isFullRank: Boolean = internal.isFullRank

    val H: Matrix = Matrix.fromArrays(internal.getH.getArray)

    val R: Matrix = Matrix.fromArrays(internal.getR.getArray)

    val Q: Matrix = Matrix.fromArrays(internal.getQ.getArray)

    def solve(that: Matrix): Matrix = {
      Matrix.fromArrays(
        internal.solve(
          new JM(that.toArrays)
        ).getArray
      )
    }

  }

  def main(args: Array[String]): Unit = {
    println("Hello World!")

//    val m = Matrix.fromRows(IndexedSeq(
//      IndexedSeq(0, 0,  1, 3),
//      IndexedSeq(1, 2,  3, 5),
//      IndexedSeq(0, 1, -2, 4),
//    ))

//    val m = Matrix.fromRows(IndexedSeq(
//      IndexedSeq(-1, 0,  0, 3),
//      IndexedSeq( 0, 1,  0, 2),
//      IndexedSeq( 0, 0, -2, 4),
//    ))

//    val m = Matrix.fromRows(IndexedSeq(
//      IndexedSeq( 1, 0,  3,  2),
//      IndexedSeq(-1, 1,  5, -4),
//      IndexedSeq( 0, 1,  3,  0),
//    ))

//    val m = Matrix.fromRows(IndexedSeq(
//      IndexedSeq( 1, 2,  1),
//      IndexedSeq(-2, 3,  5),
//    ))

    val m = Matrix.fromRows(IndexedSeq(
      IndexedSeq(1, 2, 3, 4),
      IndexedSeq(0, 0, 1, 3),
      IndexedSeq(0, 0, 0, 1),
      IndexedSeq(0, 0, 0, 0),
      IndexedSeq(0, 0, 0, 0),
    ))

    val e = m.echelonForm
    println("finsished echelonForm\n")
    val re = m.reducedEchelonForm
    println("finsished reducedEchelonForm\n")

    println("Goodbye World!")
  }

}

// Want immutability, so taking performance hit of translating IndexedSeq's into Jama.Matrix...
// provide immutable arguments for this wrapper class
// avoid allowing access to internal (wrapped) class to avoid breaking immutability
// provide both named and symbolic methods (since scala can :) )
// m x n means rows x cols
case class Matrix(m: IndexedSeq[IndexedSeq[Double]]) {
  import Matrix._
  require(m.nonEmpty, "a matrix must have at least one row")
  require(m.forall(_.size == numCols), s"not all rows have the same length: ${m.map(_.size).mkString(", ")}")

  def numRows: Int = {
    m.size
  }

  def numCols: Int = {
    m(0).size
  }

  def row(row: Int): IndexedSeq[Double] = {
    require(0 <= row && row < numRows)
    m(row)
  }

  def col(col: Int): IndexedSeq[Double] = {
    require(0 <= col && col < numCols)
    m.map(_(col))
  }

  def apply(row: Int, col: Int): Double = {
    require(0 <= row && row < numRows && 0 <= col && col < numCols, s"cannot get element ($row, $col) from ${this.numRows}x${this.numCols} matrix")
    this.row(row)(col)
  }

  def set(row: Int, col: Int, d: Double): Matrix = {
    require(0 <= row && row < numRows && 0 <= col && col < numCols, s"cannot set element ($row, $col) from ${this.numRows}x${this.numCols} matrix")
    Matrix.fromRows(
      (rows.take(row) :+
        ((this.row(row).take(col) :+ d) ++ this.row(row).drop(col+1))) ++
        rows.drop(row+1)
    )
  }

  def rows: IndexedSeq[IndexedSeq[Double]] = {
    m
  }

  def cols: IndexedSeq[IndexedSeq[Double]] = {
    this.transpose.rows
  }

  def unary_- : Matrix = this * -1
  def +(that: Matrix): Matrix = this.plus(that)
  def -(that: Matrix): Matrix = this + -that
  def *(d: Double): Matrix = this.times(d)
  def *:(d: Double): Matrix = this * d
  def *(that: Matrix): Matrix = this.times(that)

  def transform(indexFunc: (Int, Int) => Double): Matrix = {
    // this is more concise when we know the resulting matrix will have the same size as this one
    Matrix.fromIndexFunc(numRows, numCols, indexFunc)
  }

  def plus(that: Matrix): Matrix = {
    require(this.numRows == that.numRows && this.numCols == that.numCols, s"matrices must have same dimensions for addition, cannot add ${this.numRows}x${this.numCols} and ${that.numRows}x${that.numCols}")
    Matrix.fromIndexFunc(this.numRows, this.numCols, (r: Int, c: Int) => this(r, c) + that(r, c))
  }

  def times(d: Double): Matrix = {
    Matrix.fromIndexFunc(this.numRows, this.numCols, (r: Int, c: Int) => d * this(r, c))
  }

  private def dot(v1: IndexedSeq[Double], v2: IndexedSeq[Double]): Double = {
    require(v1.size == v2.size, s"dot product is only defined for vectors of the same length, not for ${v1.size} and ${v2.size}")
    (0 until v1.size).map(i => v1(i) * v2(i)).sum
  }

  def times(that: Matrix): Matrix = {
    require(this.numCols == that.numRows, s"cannot multiply a ${this.numRows}x${this.numCols} matrix by a ${that.numRows}x${that.numCols} matrix")
    Matrix.fromIndexFunc(this.numRows, that.numCols, (r, c) => dot(this.row(r), that.col(c)))
  }

  def eltTimes(that: Matrix): Matrix = {
    require(this.numRows == that.numRows && this.numCols == that.numCols, s"matrices must have same dimensions for element-wise multiplication, cannot element-wise multiply ${this.numRows}x${this.numCols} and ${that.numRows}x${that.numCols}")
    transform((r, c) => this(r, c) * that(r, c))
  }

  def eltDivideBy(that:Matrix): Matrix = {
    require(this.numRows == that.numRows && this.numCols == that.numCols, s"matrices must have same dimensions for element-wise division, cannot element-wise divide ${this.numRows}x${this.numCols} and ${that.numRows}x${that.numCols}")
    transform((r, c) => this(r, c) / that(r, c))
  }

  def isIdentity: Boolean = {
    // the iterators should let this short-circuit
    (numRows == numCols) &&
    (0 until numRows).iterator.map { r =>
      (0 until numCols).iterator.map { c =>
        ((r != c) && (this(r, c) == 0.0)) || ((r == c) && (this(r, c) == 1.0))
      }.forall(x => x)
    }.forall(x => x)
  }

  def subMatrix(rows: IndexedSeq[Int], cols: IndexedSeq[Int]): Matrix = {
    require(rows.forall(r => 0 <= r && r < numRows), s"subMatrix() cannot get rows ${rows.mkString(", ")} and cols ${cols.mkString(", ")} from ${numRows}x${numCols} matrix")
    Matrix(
      rows.map { r =>
        cols.map { c =>
          this (r, c)
        }
      }
    )
  }

  def determinant: Double = {
    Matrix.determinant(this)
  }

  def augmentCols(that: Matrix): Matrix = {
    require(this.numRows == that.numRows, s"cannot augment columns of ${this.numRows}x${this.numCols} matrix with columns from ${that.numRows}x${that.numCols} matrix")
    Matrix.fromCols(this.cols ++ that.cols)
  }

  def augmentRows(that: Matrix): Matrix = {
    require(this.numCols == that.numCols, s"cannot augment rows of ${this.numRows}x${this.numCols} matrix with rows from ${that.numRows}x${that.numCols} matrix")
    Matrix.fromRows(this.rows ++ that.rows)
  }

  def sum: Double = {
    toArrays.map(_.sum).sum
  }

  def toArrays: Array[Array[Double]] = {
    m.map(_.toArray).toArray
  }

  def transpose: Matrix = {
    Matrix.fromIndexFunc(numCols, numRows, (r, c) => this(c, r))
  }

  def cond: Double = {
    new JM(this.toArrays).cond()
  }

  def det: Double = {
    new JM(this.toArrays).det()
  }

  def norm1: Double = {
    new JM(this.toArrays).norm1()
  }

  def norm2: Double = {
    new JM(this.toArrays).norm2()
  }

  def normF: Double = {
    new JM(this.toArrays).normF()
  }

  def normInf: Double = {
    new JM(this.toArrays).normInf()
  }

  def rank: Double = {
    new JM(this.toArrays).rank()
  }

  def trace: Double = {
    new JM(this.toArrays).trace()
  }

  def swapRows(r1: Int, r2: Int): Matrix = {
    require(0 <= r1 && r1 < numRows && 0 <= r2 && r2 < numRows, s"can't swap rows $r1 and $r2 in matrix with $numRows rows")
    val lo = min(r1, r2)
    val hi = max(r1, r2)
    Matrix.fromRows(
      (rows.slice(0, lo) :+
        row(hi)) ++
      (rows.slice(lo+1, hi) :+
        row(lo)) ++
      rows.slice(hi+1, rows.size)
    )
  }

  def multiplyRow(r: Int, multiplier: Double): Matrix = {
    require(0 <= r && r < numRows, s"can't multiply rows $r in matrix with $numRows rows")
    Matrix.fromRows(
      (rows.slice(0, r) :+
        row(r).map(_ * multiplier)) ++
      rows.slice(r+1, rows.size)
    )
  }

  def addMultipleOfRow(toRow: Int, multiplier: Double, multipleOfRow: Int): Matrix = {
    require(0 <= toRow && toRow < numRows && 0 <= multipleOfRow && multipleOfRow < numRows, s"can't swap rows $toRow and $multipleOfRow in matrix with $numRows rows")
    // toRow = toRow + multiplier * multipleOfRow
    Matrix.fromRows(
      (rows.slice(0, toRow) :+
        row(toRow).zip(row(multipleOfRow)).map{ case (v, mv) => v + mv * multiplier }) ++
      rows.slice(toRow+1, rows.size)
    )
  }

  def echelonForm: Matrix = {
    // definition:
    //   * the first non-zero element in each row (the "leading entry") is 1
    //   * each leading entry is in a column to the right of the leading entry in the previous row
    //   * rows with all zero elements (if any) are below rows having a non-zero element
    var result = this
    var prevLeadPos = -1
    for (r <- 0 until numRows) {
      // loop invariants:
      //   at the end of the loop, all rows above r in the matrix (those with lower indices) are in echelon form
      //   at the end of the loop, all rows below r will have all zeros down and to the left of r's lead position
      // find a row for position r with leadPos == prevLeadPos+1 or else the min remaining leadPos (row at position r, might stay)
      val leadPosRowIndex = result.rows.zipWithIndex.drop(r).map{ case (row, rowIndex) =>
        val leadPos = row.indexWhere(_ != 0.0)
        (if (leadPos == -1) numCols else leadPos, rowIndex)
      }
      val (leadPos, newRowR) =
        leadPosRowIndex.find{ case (leadPos, _) => leadPos == prevLeadPos+1 }.
        getOrElse{ leadPosRowIndex.minBy{ case (leadPos, _) => leadPos } }
      if (newRowR != r) {
        result = result.swapRows(r, newRowR)
      }
      if (leadPos < numCols) { // else the row is all zeros... (and so are all rows below)
        result = result.multiplyRow(r, 1.0 / result(r, leadPos))
        for (belowR <- r + 1 until numRows) {
          result = result.addMultipleOfRow(belowR, -result(belowR, leadPos) / result(r, leadPos), r)
        }
      }
      prevLeadPos = leadPos
    }
    result
  }

  def reducedEchelonForm: Matrix = {
    // definition:
    //   * in echelon form
    //   * the leading entry in each row is the only non-zero entry in it's column
    var result = echelonForm
    val leadPosRowIndex = result.rows.zipWithIndex.map{ case (row, rowIndex) =>
      val leadPos = row.indexWhere(_ != 0.0)
      (leadPos, rowIndex)
    }.filter{ case (leadPos, _) => leadPos >= 0 }

    leadPosRowIndex.foreach { case (leadPos, rowIndex) =>
      for (aboveR <- 0 until rowIndex) {
        result = result.addMultipleOfRow(aboveR, -result(aboveR, leadPos) / result(rowIndex, leadPos), rowIndex)
      }
    }
    result
  }

  def inverse: Option[Matrix] = {
    require(numRows == numCols, s"can't invert ${numRows}x${numCols} matrix")
    val identity = Matrix.identity(numRows)
    val augmentedInversion = this.augmentCols(identity).reducedEchelonForm
    val augLeft= augmentedInversion.subMatrix(0 until numRows, 0 until numCols)
    if (augLeft != identity) {
      None
    } else {
      Some(augmentedInversion.subMatrix(0 until numRows, numCols until 2*numCols))
    }
  }

  def solve(that: Matrix): Matrix = {
    Matrix.fromArrays(new JM(this.toArrays).solve(new JM(that.toArrays)).getArray)
  }

  def eigenvalueDecomposition: EigenvalueDecomposition = {
    val internal = new JM(this.toArrays).eig()
    EigenvalueDecomposition(
      Matrix.fromArrays(internal.getV.getArray),
      internal.getImagEigenvalues,
      internal.getRealEigenvalues,
      Matrix.fromArrays(internal.getD.getArray))
  }

  def singularValueDecomposition: SingularValueDecomposition = {
    val internal = new JM(this.toArrays).svd()
    SingularValueDecomposition(
      Matrix.fromArrays(internal.getU.getArray),
      Matrix.fromArrays(internal.getV.getArray),
      internal.getSingularValues,
      Matrix.fromArrays(internal.getS.getArray),
      internal.norm2(),
      internal.cond(),
      internal.rank()
    )
  }

  def choleskyDecomposition: CholeskyDecomposition = {
    new CholeskyDecomposition(new JM(this.toArrays).chol())
  }

  def luDecomposition: LUDecomposition = {
    new LUDecomposition(new JM(this.toArrays).lu())
  }

  def qrDecomposition: QRDecomposition = {
    new QRDecomposition(new JM(this.toArrays).qr())
  }

  override def toString(): String = {
    val formatted = rows.map{ row =>
      row.map(v => f"$v%.2f")
    }
    val maxLength = formatted.map(_.map(_.size).max).max
    // pad with leading spaces to align decimals
    formatted.map { formattedRow =>
      formattedRow.map(s => " " * (maxLength - s.size) + s).mkString("| ", " ", " |")
    }.mkString("\n")
  }

}
