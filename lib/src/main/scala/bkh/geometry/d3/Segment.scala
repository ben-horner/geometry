package bkh.geometry.d3

case class Segment(p1: Pnt, p2: Pnt) extends Transformable[Segment] {

  override def transform(transformation: Transform): Segment = {
    Segment(transformation(p1), transformation(p2))
  }

  def toLine: Line = Line(p1, p2-p1)

  def midpoint: Pnt = p1.midPoint(p2)

  def perpBisector: Plane = Plane(midpoint, (p2 - p1))

}
