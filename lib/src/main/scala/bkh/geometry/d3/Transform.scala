package bkh.geometry.d3

import bkh.matrix.Matrix

import scala.math.{cos, sin}

case class Transform(transformation: Matrix) extends Transformable[Transform] {
  require(transformation.numRows == 4 && transformation.numCols== 4, s"transformation matrix should be 4x4, got ${transformation.numRows}x${transformation.numCols}")

  // the transformation closest to the vector being transformed happens first
  // this takes advantage of the distributive property and multiplies the transformations before the vector is known
  // (T1 * (T2 * v)) == ((T1 * T2) * v)
  def andThen(that: Transform): Transform = { // this.andThen(that)
    Transform(that.transformation * this.transformation)
  }

  def after(that: Transform): Transform = { // this.after(that)
    Transform(this.transformation * that.transformation)
  }

  def apply(pnt: Pnt): Pnt = {
    val transformed = this.transformation * Matrix.fromCol(pnt.toArray :+ 1.0)
    Pnt(transformed(0, 0), transformed(1, 0), transformed(2, 0))
  }

  override def transform(that: Transform): Transform = {
    // by transforming this transformation, we are specifying that the resulting transformation should be
    // equivalent to first applying this transformation, and then applying that transformation
    // transformable.transform(this.transform(that)) is equivalent to transformable.transform(this).transform(that)
    this.andThen(that)
  }

  override def toString: String = s"Transformation(\n$transformation)"

}

object Transform {

  val identity: Transform = Transform(Matrix.identity(4))

  // translation: | 1 0 0 dx |   | x |   | x + dx |
  //              | 0 1 0 dy | * | y | = | y + dy |
  //              | 0 0 1 dz |   | z |   | z + dz |
  //              | 0 0 0  1 |   | 1 |   |   1    |
  def translate(v: Vec): Transform = {
    Transform(Matrix.identity(3).
      augmentCols(Matrix.fromCol(v.toArray)).
      augmentRows(Matrix.fromRow(Array(0.0, 0.0, 0.0, 1.0)))
    )
  }

  /*****************************************************************************************
   * you could call these convenience methods, but there are performance concerns          *
   * they produce a single transformation which can be applied once to each of many points *
   * rather that transforming each point many times                                        *
   *****************************************************************************************/
  def scale(scaleXYZ: Double, center: Pnt): Transform = {
    this.scale(scaleXYZ, scaleXYZ, scaleXYZ, center)
  }

  def scale(scaleX: Double, scaleY: Double, scaleZ: Double, center: Pnt): Transform = {
    translate(-center.v).andThen(scale(scaleX, scaleY, scaleZ)).andThen(translate(center.v))
  }

  def pointReflect(center: Pnt): Transform = {
    translate(-center.v).
      andThen(originReflect).
      andThen(translate(center.v))
  }

  def linearReflect(line: Line): Transform = {
    translate(-line.intercept.v).
      andThen(linearReflect(line.dir)).
      andThen(translate(line.intercept.v))
  }

  def planarReflect(plane: Plane): Transform = {
    translate(-plane.intercept.v).
      planarReflect(plane.perp).
      translate(plane.intercept.v)
  }

  def rotate(degrees: Double, axis: Line): Transform = {
    rotateRadians(degrees * math.Pi / 180.0, axis)
  }

  def rotateRadians(radians: Double, axis: Line): Transform = {
    translate(-axis.intercept.v).
      andThen(rotateRadians(radians, axis.dir)).
      andThen(translate(axis.intercept.v))
  }


  /********************************************************
   * transformations centered at origin                   *
   * these are the ones that manipulate matrices directly *
   ********************************************************/
  def scale(scaleXYZ: Double): Transform = {
    this.scale(scaleXYZ, scaleXYZ, scaleXYZ)
  }

  // scale: | sx  0  0 0 |   | x |   | sx*x |
  //        |  0 sy  0 0 | * | y | = | sy*y |
  //        |  0  0 sz 0 |   | z |   | sz*z |
  //        |  0  0  0 1 |   | 1 |   |   1  |
  def scale(scaleX: Double, scaleY: Double, scaleZ: Double) = {
    Transform(Matrix.fromRows(IndexedSeq(
      IndexedSeq(scaleX,    0.0,    0.0, 0.0),
      IndexedSeq(   0.0, scaleY,    0.0, 0.0),
      IndexedSeq(   0.0,    0.0, scaleZ, 0.0),
      IndexedSeq(   0.0,    0.0,    0.0, 1.0),
    )))
  }

  // point reflection: | -1  0  0 0 |   | x |   | -x |
  //                   |  0 -1  0 0 | * | y | = | -y |
  //                   |  0  0 -1 0 |   | z |   | -z |
  //                   |  0  0  0 1 |   | 1 |   |  1 |
  def originReflect: Transform = { // reflects through origin
    scale(-1)
  }

  def linearReflect(dir: Vec): Transform = { // reflects across line through origin with dir
    rotate(dir, Vec.Z).
      andThen(zAxisReflect).
      andThen(rotate(Vec.Z, dir))
  }

  // just like origin reflect in 2d, x and y go negative, z stays the same
  // | -1  0  0  0 |   | x |   | -x |
  // |  0 -1  0  0 | * | y | = | -y |
  // |  0  0  1  0 |   | z |   |  z |
  // |  0  0  0  1 |   | 1 |   |  1 |
  def zAxisReflect: Transform = {
    Transform(Matrix.fromRows(IndexedSeq(
      IndexedSeq(-1.0,  0.0,  0.0,  0.0),
      IndexedSeq( 0.0, -1.0,  0.0,  0.0),
      IndexedSeq( 0.0,  0.0,  1.0,  0.0),
      IndexedSeq( 0.0,  0.0,  0.0,  1.0),
    )))
  }

  // looks like this is for a plane:  https://math.stackexchange.com/questions/952092/reflect-a-point-about-a-plane-using-matrix-transformation
  // I - u * uT

  // | 1 0 0 |   | x |               | 1-xx  -xy  -xz |
  // | 0 1 0 | - | y | * | x y z | = |  -yx 1-yy  -yz |
  // | 0 0 1 |   | z |               |  -zx  -zy 1-zz |
  def planarReflect(perp: Vec): Transform = {
    val u = perp.unit
    Transform(
      Matrix.identity(3) - Matrix.fromRow(u.toArray) * Matrix.fromCol(u.toArray).
        augmentCols(Matrix.zero(3, 1)).
        augmentRows(Matrix.fromRow(Array(0.0, 0.0, 0.0, 1.0)))
    )
  }

  // there is a dependency between Vec.angleFrom() and this.rotate(Vec, Vec)
  def rotate(fromDir: Vec, toDir: Vec): Transform = {
    // there is a dependency between angleFrom() and cross() here

    // the two possibilities fromDir.cross(toDir) and toDir.cross(fromDir)
    // will produce parallel vectors with opposite signs
    // the direction of rotation will be opposite for those two possibilities (right hand rule)

    // angleFrom could theoretically produce two values x and 360-x
    // it will actually produce whichever is between -180 and 180
    val degrees = toDir.angleFrom(fromDir)
    rotate(fromDir, toDir, degrees)
  }

  def rotate(fromDir: Vec, toDir: Vec, degrees: Double): Transform = {
    require((degrees == 0.0) || (fromDir != toDir), "must have two different vectors to define a rotation")
    require(fromDir != (-toDir), "rotation between opposite vectors is undefined")
    if (degrees == 0.0) {
      Transform(Matrix.identity(4))
    } else {
      rotate(degrees, fromDir.cross(toDir))
    }
  }

  def rotate(degrees: Double, axis: Vec): Transform = {
    rotateRadians(degrees * math.Pi / 180.0, axis)
  }


  // if the two sequences of points are not compatible, then there is no transformation that aligns them perfectly
  // if they can be aligned perfectly, the following conditions will do so, if not, they describe the error
  // returns a transformation such that for [(o)riginal and (t)ransformed]:
  // * o1 will be transformed into the exact position of t1
  // * o2 will be transformed such that the vector o2-o1 points in the exact direction of t2-t1 (so o1, o2, t1 and t2 will be collinear)
  // * o3 will be transformed such that o* and t* are coplanar, and o3 and t3 will be on the same side of the line containing (o1, o2, t1, t2)
  def alignThree(originalThree: Seq[Pnt], transformedThree: Seq[Pnt]): Transform = {
    require(originalThree.size == 3)
    require(transformedThree.size == 3)

    val vertex0ToOrigin: Vec = -originalThree(0).v
    val translateOrigin: Transform = translate(vertex0ToOrigin)

    val originTranslatedThree: Seq[Pnt] = originalThree.map(_.transform(translateOrigin))

    val edge01Dir: Vec = originTranslatedThree(1) - originTranslatedThree(0)
    val final01Dir: Vec = transformedThree(1) - transformedThree(0)
    val alignEdge01: Transform = if (-edge01Dir != final01Dir) {
      rotate(edge01Dir, final01Dir)
    } else {
      val originalNorm: Vec = (originalThree(1) - originalThree(0)).cross(originalThree(2) - originalThree(0))
      rotate(180, originalNorm)
    }

    val edge01AlignedThree: Seq[Pnt] = originTranslatedThree.map(_.transform(alignEdge01))

    val edge01AlignedNorm: Vec = (edge01AlignedThree(1) - edge01AlignedThree(0)).cross(edge01AlignedThree(2) - edge01AlignedThree(0))
    val finalNorm: Vec = (transformedThree(1) - transformedThree(0)).cross(transformedThree(2) - transformedThree(0))
    val alignNorm: Transform = if (-edge01AlignedNorm != finalNorm) {
      rotate(edge01AlignedNorm, finalNorm)
    } else {
      rotate(180, edge01AlignedThree(1) - edge01AlignedThree(0))
    }

    val normAlignedVertex0ToFinal: Vec = transformedThree(0).v
    val translateFinal = translate(normAlignedVertex0ToFinal)

    identity.
      andThen(translateOrigin).
      andThen(alignEdge01).
      andThen(alignNorm).
      andThen(translateFinal)
  }


  // from here:  https://en.wikipedia.org/wiki/Rotation_matrix#Axis_and_angle
  // and here:  https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
  // unit axis u = <x, y, z>
  // cos(t)*I + sin(t)*[u]x + (1-cos(t))*u(x)u

  // c = cos t
  // s = sin t
  // C = 1-c

  // rotation around unix axis:
  // | xxC+c  xyC-zs xzC+ys 0 |   | x' |
  // | yxC+zs yyC+c  yzC-xs 0 | * | y' | = ...4x1
  // | zxC-ys zyC+xs zzC+c  0 |   | z' |
  // |   0      0      0    1 |   | 1  |

  //   | 1 0 0 |     |  0 -z  y |     | x |
  // c*| 0 1 0 | + s*|  z  0 -x | + C*| y |*| x y z |
  //   | 0 0 1 |     | -y  x  0 |     | z |

  def crossProductMatrix(v: Vec): Matrix = {
    Matrix.fromRows(IndexedSeq(
      IndexedSeq( 0.0, -v.z,  v.y),
      IndexedSeq( v.z,  0.0, -v.x),
      IndexedSeq(-v.y,  v.x,  0.0)
    ))
  }

  def rotateRadians(radians: Double, axis: Vec): Transform = {
    val u = axis.unit
    val c = cos(radians)
    val s = sin(radians)
    val C = 1 - c

    val m =
      c *: Matrix.identity(3) +
      s *: crossProductMatrix(u)    +
      (C *: Matrix.fromCol(u.toArray)) * Matrix.fromRow(u.toArray)

    Transform(m.augmentCols(Matrix.zero(3, 1)).
      augmentRows(Matrix.fromRow(Array(0.0, 0.0, 0.0, 1.0)))
    )
  }

}
