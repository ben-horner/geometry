package bkh.geometry.d3

import scala.math.abs

case class Plane private (intercept: Pnt, perp: Vec) extends Transformable[Plane] {
  require(abs(1.0 - perp.length) < 0.0000000001, s"perpendicular to the plane was not a unit vector: $perp")
  require(intercept.dist(nearestTo(Pnt.ORIGIN)) < 0.0000000001)

  override def transform(transformation: Transform): Plane = {
    val p0 = intercept
    val p1 = intercept + perp
    val p0t = transformation(p0)
    val p1t = transformation(p1)
    Plane(p0t, p1t - p0t)
  }

  def dist(p: Pnt): Double = (intercept - p).projParallel(perp).length

  def dist(line: Line): Double = {
    if (this.perp.angleFrom(line.dir) == 90.0) {
      this.dist(line.intercept)
    } else {
      0.0
    }
  }

  def dist(that: Plane): Double = {
    if (this.perp == that.perp) {
      this.dist(that.intercept)
    } else {
      0.0
    }
  }

  def nearestTo(p: Pnt): Pnt = p + (intercept - p).projParallel(perp)

  // from here: http://geomalgorithms.com/a05-_intersect-1.html
  def intersect(line: Line): Option[Pnt] = {
    if (this.perp.angleFrom(line.dir) == 90.0) {
      None
    } else {
      val s = -this.perp.dot(line.intercept - this.intercept) / this.perp.dot(line.dir)
      Some(line.intercept + s *: line.dir)
    }
  }

  def intersect(that: Plane): Option[Line] = {
    if (this.perp.angleFrom(that.perp) == 0.0) {
      None
    } else {
      val intersectionDir = this.perp.cross(that.perp)
      val inThatPerpToIntersectionDir = intersectionDir.cross(that.perp)
      val inThatPerpToIntersection = Line(that.intercept, inThatPerpToIntersectionDir)
      Some(Line(this.intersect(inThatPerpToIntersection).get, intersectionDir))
    }
  }

}

object Plane {

  val XY = Plane(Pnt.ORIGIN, Vec.Z)
  val XZ = Plane(Pnt.ORIGIN, Vec.Y)
  val YZ = Plane(Pnt.ORIGIN, Vec.X)

  def apply(intercept: Pnt, perp: Vec): Plane = {
    new Plane(intercept.v.projParallel(perp).toPnt, perp.unit)
  }

  def apply(p1: Pnt, p2: Pnt, p3: Pnt): Plane = {
    require(p1 != p2 && p1 != p3 && p2 != p3)
    Plane.apply(p1, (p2-p1).cross(p3-p1))
  }

}
