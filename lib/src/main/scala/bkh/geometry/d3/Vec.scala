package bkh.geometry.d3

import bkh.matrix.Matrix

import scala.math._

case class Vec(x: Double, y: Double, z: Double) extends Transformable[Vec] {
  def unary_- : Vec = Vec(-x, -y, -z)
  def unary_+ : Vec = this
  def * (scalar: Double): Vec = Vec(this.x * scalar, this.y * scalar, this.z * scalar)
  def *:(scalar: Double): Vec = this * scalar
  def / (scalar: Double): Vec = this * (1 / scalar)
  def +(that: Vec): Vec = Vec(this.x + that.x, this.y + that.y, this.z + that.z)
  def -(that: Vec): Vec = this + -that
  def dot(that: Vec): Double = this.x * that.x + this.y * that.y + this.z * that.z
  def *(that: Vec): Double = this.dot(that)
  def cross(that: Vec): Vec = {
    val sx = this.y * that.z - this.z * that.y
    val sy = this.z * that.x - this.x * that.z
    val sz = this.x * that.y - this.y * that.x
    Vec(sx, sy, sz)
  }
  def x(that: Vec): Vec = this.cross(that)
  def unit: Vec = {
    val result = this/length
    assume(abs(1.0 - result.length) < 0.0000000001, s"Couldn't produce unit vector for $this")
    result
  }
  def length: Double = sqrt(this.dot(this))
  def withLength(length: Double): Vec = this.unit * length

  // -180 <= angle <= 180 (cross product determines which direction is positive rotation)
  def angleFrom(that: Vec): Double = radiansFrom(that) * 180 / Pi
  // -Pi <= radians <= Pi (cross product determines which direction is positive rotation)
  def radiansFrom(that: Vec): Double = atan2(that.cross(this).length, that.dot(this))
  def scalarProj(that: Vec): Double = this.dot(that.unit)
  def projParallel(that: Vec): Vec = that.unit * this.scalarProj(that)
  def projPerp(that: Vec): Vec = this - projParallel(that)

  override def transform(transformation: Transform): Vec = {
    transformation(toPnt).v
  }

  // TODO: validate this... is this actually a correct way to maintain all rotations etc but remove translations?
  def noTranslation(transformation: Transform): Vec = {
    val m = transformation.transformation
    val translationless = Transform(
      m.subMatrix(0 until m.numRows, 0 until m.numCols-1).
        augmentCols(Matrix.fromCol(IndexedSeq(0.0, 0.0, 0.0, 1.0)))
    )
    transform(translationless)
  }

  // from https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
  // r = sqrt(x^2 + y^2 + z^2)    0 <= r
  // latitude = 90-acos(z / r)  -90 <= latitude  <= 90
  // longitude = atan2(y, x)   -180 <= longitude <= 180
  def toSpherical: (Double, Double, Double) = {
    val (r, latitudeRadians, longitudeRadians) = toSphericalRadians
    (r, 180.0 * latitudeRadians / Pi, 180.0 * longitudeRadians / Pi)
  }

  def toSphericalRadians: (Double, Double, Double) = {
    if (this == Vec.ZERO) {
      (0.0, 0.0, 0.0)
    } else {
      val r = sqrt(x * x + y * y + z * z)
      val latitude = Pi/2 - acos(z / r)
      val longitude = atan2(y, x)
      (r, latitude, longitude)
    }
  }

  def toPnt: Pnt = Pnt(this)

  def toArray: Array[Double] = Array(x, y, z)

  override def toString: String = s"<$x,$y,$z>"
}

object Vec {
  val ZERO: Vec = Vec(0, 0, 0)
  val X: Vec = Vec(1, 0, 0)
  val Y: Vec = Vec(0, 1, 0)
  val Z: Vec = Vec(0, 0, 1)

  def from(array: Array[Double]): Vec = {
    require(array.size == 3)
    Vec(array(0), array(1), array(2))
  }

  // from https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates
  // x = r * sin(latitude) * cos(longitude)
  // y = r * sin(latitude) * sin(longitude)
  // z = r * cos(latitude)
  def fromSpherical(spherical: (Double, Double, Double)): Vec = {
    fromSpherical(spherical._1, spherical._2, spherical._3)
  }

  def fromSpherical(r: Double, latitude: Double, longitude: Double): Vec = {
    fromSphericalRadians(r, Pi * latitude / 180.0, Pi * longitude / 180.0)
  }

  def fromSphericalRadians(r: Double, latitude: Double, longitude: Double): Vec = {
    if (r == 0.0 && latitude == 0.0 && longitude == 0.0) {
      Vec.ZERO
    } else {
      val x = r * sin(Pi/2-latitude) * cos(longitude)
      val y = r * sin(Pi/2-latitude) * sin(longitude)
      val z = r * cos(Pi/2-latitude)
      Vec(x, y, z)
    }
  }

  def main(args: Array[String]): Unit = {
    println("Hello World!")
    val v1 = Vec(1, 1, 0)
    val v2 = Vec(1, 0, 0)
    println(v1.projParallel(v2).radiansFrom(v2))
    // counterclockwise should be positive
    println(v1.angleFrom(v2))
    println(v2.angleFrom(v1))

    val testPts = for {
      x <- Iterator(-1.0, 0.0, 1.0)
      y <- Iterator(-1.0, 0.0, 1.0)
      z <- Iterator(-1.0, 0.0, 1.0)
      if (x != 0.0 || y != 0.0 || z != 0.0)
    } yield Vec(x, y, z)

    testPts.toSeq.sortBy(_.length).foreach { v =>
      val spherical = v.toSpherical
      val rectangular = Vec.fromSpherical(spherical)
      if ((v - Vec.fromSpherical(v.toSpherical)).length > 0.000000001) print("ERROR:  ")
      println(s"$v --> ${v.unit.toSpherical} --> ${Vec.fromSpherical(v.toSpherical)}")
    }

  }
}
