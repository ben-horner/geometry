package bkh.geometry.d3

import java.io.{FileWriter, PrintWriter}

// TODO: would really like to ensure that orientation of faces are all correct, including first and last for non-cycles
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Primitive_Solids#Debugging_polyhedra
object Extrusions {

  // extrusions should extrude perpendicular to each segment
  // where segments meet, there is a plane which bisects the angle between the segments
  // the meeting of the segments should be the projection of the extrusion outline onto this plane
  // (once the plane is determined, the projection should match when projecting from either incident segment)

  // path projections:
  // initial segment should have cap at first point, perpendicular to first segment
  // final segment should have a cap at the last point, perpendicular to the last segment

  // cyclic projections
  // cycles have no caps
  // a cycle has a special segment which joins the final point back to the first
  // the final intersection plane prior to this special segment depends on the direction of that special segment

  def extrudeAlong(outline: Seq[Pnt], position: Pnt, forward: Vec, up: Vec, path: Seq[Pnt]): Polyhedron = {
    require(path.size >= 2, s"nothing to extrude, direction is ambiguous for path of length ${path.size}")
    val frame = Frame(outline, position, forward, up)
    val initialDir = path(1) - path(0)
    val finalDir = path.last - path.init.last

    val images = extrudeImages(frame, initialDir, path, finalDir)
    val faces = joinImages(images) :+ images.head :+ images.last.reverse // add cap faces
    Polyhedron(faces)
  }

  def extrudeAlongCycle(outline: Seq[Pnt], position: Pnt, forward: Vec, up: Vec, cycle: Seq[Pnt]): Polyhedron = {
    require(cycle.size >= 3, s"cannnot have a cyle of length ${cycle.size}")
    require(cycle.head != cycle.last, s"extrudeAlongCycle() is designed for open cycles, shouldn't start and end at same point: ${cycle.head}")
    val frame = Frame(outline, position, forward, up)
    val initialDir = intersectionPlane(cycle.last, cycle.head, cycle.tail.head).perp
    val finalDir = intersectionPlane(cycle.init.last, cycle.last, cycle.head).perp

    val images = extrudeImages(frame, initialDir, cycle, finalDir)
    val faces = joinImages(images :+ images.head) // no cap faces for cycle...
    Polyhedron(faces)
  }

  case class Frame(outline: Seq[Pnt], position: Pnt, forward: Vec, up: Vec) extends Transformable[Frame] {

    override def transform(transformation: Transform): Frame = {
      Frame(
        outline.map(_.transform(transformation)),
        position.transform(transformation),
        forward.noTranslation(transformation),
        up.noTranslation(transformation)
      )
    }

    def reposition(newPos: Pnt, newForward: Vec): Frame = {
      val localUp = Vec.Z.rotate(Vec.Z, newForward, -(90 - Vec.Z.angleFrom(newForward)))

      var result = this.rotate(this.forward, newForward)
      result = result.rotate(result.up, localUp)
      result = result.translate(newPos - result.position)
      result
    }

    def image(projectionPlane: Plane): Seq[Pnt] = {
      outline.map{ p =>
        projectionPlane.intersect(Line(p, forward)).get
      }
    }

  }

  // initialDir is both the initial projection direction and the perpendicular to the initial projection plane
  // finalDir is both the final projection direction and the perpendicular to the final projection plane
  def extrudeImages(frame: Frame, initialDir: Vec, path: Seq[Pnt], finalDir: Vec): Seq[Seq[Pnt]] = {
    require(path.size >= 2, s"can't extrude with a path of less than two points: [${path.mkString(", ")}]")
    require(path.size < 3 || path.sliding(3).forall{ case Seq(a, b, c) => Set(a, b, c).size == 3 })

    val incomingDirs = (Iterator(initialDir) ++
      path.dropRight(1).sliding(2).dropWhile(_.size < 2).map{ case Seq(p1, p2) => p2-p1 } ++
      Iterator(finalDir)).toIndexedSeq
    require(incomingDirs.size == path.size)

    val projectionPlanes = (Iterator(Plane(path.head, initialDir)) ++
      path.sliding(3).dropWhile(_.size < 3).map{ case Seq(p1, p2, p3) => intersectionPlane(p1, p2, p3) } ++
      Iterator(Plane(path.last, finalDir))).toIndexedSeq
    require(projectionPlanes.size == path.size)

    val images = for { i <- path.indices } yield {
      frame.reposition(path(i), incomingDirs(i)).image(projectionPlanes(i))
    }
    require(images.size == path.size)

    images
  }

  def joinImages(images: Seq[Seq[Pnt]]): Seq[Seq[Pnt]] = {
    val polyedralFaces = images.sliding(2).flatMap { case Seq(image1, image2) =>
      require(image1.size == image2.size, s"can't join images of differnet lengths ${image1.size} and ${image2.size}")
      val zipped = image1.zip(image2)
      (zipped :+ zipped.head).sliding(2).map { case Seq((p11, p21), (p12, p22)) =>
        Seq(p11, p21, p22, p12)
      }.toSeq
    }.toSeq
    polyedralFaces
  }

  // plane at intersection of two segments (start-intersection) and (intersection-end)
  private def intersectionPlane(start: Pnt, intersectionPoint: Pnt, end: Pnt): Plane = {
    require(Set(start, intersectionPoint, end).size == 3)
    val startDir = start - intersectionPoint
    val endDir = end - intersectionPoint
    val mutualPerp = startDir.cross(endDir)
    val intersectionPlanePerp = if (mutualPerp != Vec.ZERO) {
      val angle = endDir.angleFrom(startDir)
      val bisector = startDir.rotate(angle / 2, mutualPerp)
      mutualPerp.cross(bisector)
    } else {
      endDir
    }
    Plane(intersectionPoint, intersectionPlanePerp)
  }

  def main(args: Array[String]): Unit = {
    val outline = Seq(
      Pnt(1, 1, 0),
      Pnt(-1, 1, 0),
      Pnt(-1, -1, 0),
      Pnt(1, -1, 0)
    )
    val follower = Pnt(0, 0, 0)
    val forward = Vec(0, 0, 1)
    val up = Vec(1, 0, 0)

    import math.Pi
    val path = (BigDecimal(0.0) to BigDecimal(4*Pi) by 0.1).map{ b =>
      val a: Double = b.toDouble
      Pnt(5*math.sin(a), 5*math.cos(a), a)
    }

    val polyhedron = extrudeAlong(outline, follower, forward, up, path)
//    val solidPolyhedron = Polyhedron.toSolidPolyhedron(polyhedron)



    val pw = new PrintWriter(new FileWriter("/home/ben/git/scad-primitives/src/main/resources/polyhedron.scad"))
    pw.println("//Hello World!")

//    pw.println(solidPolyhedron.toScad.mkString("\n"))
//    pw.println(Paths.trace(path, 0.25, 0.01).toScad.mkString("\n"))

    pw.println("//Goodbye World!")
    pw.close()

  }

  // outline:  a set of edges defined by a sequence of points
  // path:  a sequence of points defining the path that the outline should slide along
  // slider:  the point relative to the outline that travels the path, carrying the outline with it

  // the edges of the outline trace out the faces of the extrusion
  // an extrusion of the outline along a single edge of the path needs to be parallel to the path edge
  // where edges meet, there is a corner (a point), extrusions meet here at a plane
  // the plane should include the angle bisector, as well as the mutual perpendicular to the two path edges

  // the outline should be translated such that the slider at the center of the first segment
  // then the outline should be projected along the edge in both directions onto the two intersection planes
  // moving forward the projection on the previous intersection plane should be projected along the following edge
  // the fact that the intersection plane split the angle between the two edges should maintain the proportionality of the outline
  // it would be nice if this was more "declarative", so that we didn't have the propagation of error in projections...
  // the intersection planes will themselves intersect at lines, those lines should clip the projection of the outline

  // a sliding window of 3 path points is needed to produce the intersection plane for the center point...

  // what about the end caps?  (should be optional in case path is closed -- a cycle)

  // define an up?  try

  // what about rotating (about axis) as we extrude?
  // what about translating (relative to axis, so 2D translations) as we extrude?
  // what about scaling (about axis) as we extrude?
  // reflecting as we extrude doesn't make sense... it's discontinuous

}
