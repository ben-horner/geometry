package bkh.geometry.d3

import scala.math.abs

class Ray private (end: Pnt, dir: Vec) extends Transformable[Ray] {
  require(abs(1.0 - dir.length) < 0.000000001)

  override def transform(transformation: Transform): Ray = {
    val p0 = end
    val p1 = p0 + dir
    val p0t = transformation(p0)
    val p1t = transformation(p1)
    Ray(p0t, p1t - p0t)
  }

  def toLine: Line = Line(end, dir)

}

object Ray {

  def apply(end: Pnt, dir: Vec): Ray = {
    // pick between dir and -dir.
    new Ray(end, dir.unit)
  }

}
