package bkh.geometry.d3

case class Polyhedron(faces: Seq[Seq[Pnt]]) extends Transformable[Polyhedron] {

  override def transform(transformation: Transform): Polyhedron =
    Polyhedron(faces.map(_.map(transformation(_))))

  def insideOut: Polyhedron = {
    Polyhedron(faces.map(_.reverse))
  }

  // TODO: find the frontier of edges on the "outside" of the surface
  // can we come up with a multi-matching to close the polyhedron? (don't introduce new points if we can help it...)
//  def patch: Polyhedron = ???

}
