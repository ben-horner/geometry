package bkh.geometry.d3

import bkh.geometry.d3.Vec.ZERO

case class Pnt(v: Vec) extends Transformable[Pnt] {
  def x: Double = v.x
  def y: Double = v.y
  def z: Double = v.z

  def +(v: Vec): Pnt = Pnt(this.v + v)
  def -(v: Vec): Pnt = Pnt(this.v - v)
  def -(that: Pnt): Vec = this.v - that.v

  def dist(that: Pnt): Double = {
    (that.v - this.v).length
  }

  override def transform(transformation: Transform): Pnt = {
    Pnt(v.transform(transformation))
  }

  def toArray: Array[Double] = v.toArray

  def midPoint(that: Pnt): Pnt = {
    Pnt(this.v + (that.v - this.v)/2)
  }

  override def toString: String = v.toString
}

object Pnt {
  val ORIGIN: Pnt = Pnt(ZERO)

  def apply(x: Double, y: Double, z: Double): Pnt = new Pnt(Vec(x, y, z))

  def from(array: Array[Double]): Pnt = {
    Pnt(Vec.from(array))
  }
}
