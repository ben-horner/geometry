package bkh.geometry.d2

trait Transformable[A <: Transformable[A]] {

  def transform(transformation: Transform): A

  // interesting transformations providing a parameter for the center (all but translate)
  def translate(vec: Vec): A = transform(Transform.translate(vec))

  def scale(scaleXY: Double, center: Pnt): A = transform(Transform.scale(scaleXY, center))
  def scale(scaleX: Double, scaleY: Double, center: Pnt): A = transform(Transform.scale(scaleX, scaleY, center))

  def pointReflect(center: Pnt): A = transform(Transform.pointReflect(center))

  def linearReflect(line: Line): A = transform(Transform.linearReflect(line))

  def rotate(degrees: Double, center: Pnt): A = transform(Transform.rotate(degrees, center))
  def rotateRadians(radians: Double, center: Pnt): A = transform(Transform.rotateRadians(radians, center))

  // transformations centered at origin
  def scale(scaleXY: Double): A = scale(scaleXY, scaleXY)
  def scale(scaleX: Double, scaleY: Double): A = transform(Transform.scale(scaleX, scaleY))
  def originReflect: A = transform(Transform.originReflect)
  def linearReflect(dir: Vec): A = transform(Transform.linearReflect(dir))
  def rotate(degrees: Double): A = transform(Transform.rotate(degrees))
  def rotateRadians(radians: Double): A = transform(Transform.rotateRadians(radians))

}
