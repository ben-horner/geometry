package bkh.geometry.d2

import bkh.geometry.d3

import scala.math.{Pi, abs, atan2, sqrt}


case class Vec(x: Double, y: Double) extends Transformable[Vec] {
  def unary_- : Vec = Vec(-x, -y)
  def unary_+ : Vec = this
  def * (scalar: Double): Vec = Vec(this.x * scalar, this.y * scalar)
  def *:(scalar: Double): Vec = this * scalar
  def / (scalar: Double): Vec = this * (1 / scalar)
  def +(that: Vec): Vec = Vec(this.x + that.x, this.y + that.y)
  def -(that: Vec): Vec = this + -that
  def dot(that: Vec): Double = this.x * that.x + this.y * that.y
  def *(that: Vec): Double = this.dot(that)
  // note that cross product is actually only defined for 3d vectors.
  // this function returns the magnitude of the cross product of these 2d vectors in 3d (where their z=0)
  def cross(that: Vec): Double = this.x * that.y - this.y * that.x
  def x(that: Vec): Double = this.cross(that)
  def unit: Vec = {
    val result = this/length
    assume(abs(1.0 - result.length) < 0.0000000001, s"Couldn't produce unit vector for $this")
    result
  }
  def length: Double = sqrt(this.dot(this))
  def withLength(length: Double) = this.unit * length

  // -180 <= angle <= 180
  def angleFrom(that: Vec): Double = radiansFrom(that) * 180 / Pi
  // -Pi <= radians <= Pi
  def radiansFrom(that: Vec): Double = atan2(that.cross(this), that.dot(this))
  def scalarProj(that: Vec): Double = this.dot(that.unit)
  def projParallel(that: Vec): Vec = that.unit * this.scalarProj(that)
  def projPerp(that: Vec): Vec = this - projParallel(that)

  override def transform(transformation: Transform): Vec = {
    // translations will change the direction of the vector... that's why transformations operate on Pnt
    transformation(toPnt).v
  }

  def toPnt: Pnt = Pnt(this)

  def to3D: d3.Vec = d3.Vec(x, y, 0.0)

  def toArray: Array[Double] = Array(x, y)

  override def toString: String = s"<$x,$y>"
}

object Vec {
  val ZERO: Vec = Vec(0, 0)
  val X: Vec = Vec(1, 0)
  val Y: Vec = Vec(0, 1)

  def from(array: Array[Double]): Vec = {
    require(array.size == 2)
    Vec(array(0), array(1))
  }

  def main(args: Array[String]): Unit = {
    println("Hello World!")
    val v1 = Vec(1, 1)
    val v2 = Vec(1, 0)
    println(v1.projParallel(v2).radiansFrom(v2))
    // counterclockwise should be positive
    println(v1.angleFrom(v2))
    println(v2.angleFrom(v1))
  }
}
