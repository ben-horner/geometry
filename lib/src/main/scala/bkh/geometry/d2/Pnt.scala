package bkh.geometry.d2

import bkh.geometry.d2.Vec.ZERO
import bkh.geometry.d3

case class Pnt(v: Vec) extends Transformable[Pnt] {
  def x: Double = v.x
  def y: Double = v.y

  def +(v: Vec): Pnt = Pnt(this.v + v)
  def -(v: Vec): Pnt = Pnt(this.v - v)
  def -(that: Pnt): Vec = this.v - that.v

  def dist(that: Pnt): Double = {
    (that.v - this.v).length
  }

  override def transform(transformation: Transform): Pnt = {
    Pnt(v.transform(transformation))
  }

  def toArray: Array[Double] = v.toArray

  def midPoint(that: Pnt): Pnt = {
    Pnt(this.v + (that.v - this.v)/2)
  }

  override def toString: String = v.toString

  def to3D: d3.Pnt = d3.Pnt(x, y, 0.0)
}

object Pnt {
  val ORIGIN: Pnt = Pnt(ZERO)

  def apply(x: Double, y: Double): Pnt = new Pnt(Vec(x, y))

  def from(array: Array[Double]): Pnt = {
    Pnt(Vec.from(array))
  }

}
