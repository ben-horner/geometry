package bkh.geometry.d2

import bkh.matrix.Matrix

case class Transform(transformation: Matrix) extends Transformable[Transform] {
  require(transformation.numRows == 3 && transformation.numCols== 3, s"transformation matrix should be 3x3, got ${transformation.numRows}x${transformation.numCols}")

  // the transformation closest to the vector being transformed happens first
  // this takes advantage of the distributive property and multiplies the transformations before the vector is known
  // (T1 * (T2 * v)) == ((T1 * T2) * v)
  def andThen(that: Transform): Transform = { // this.andThen(that)
    Transform(that.transformation * this.transformation)
  }

  def after(that: Transform): Transform = { // this.after(that)
    Transform(this.transformation * that.transformation)
  }

  def apply(pnt: Pnt): Pnt = {
    val transformed = this.transformation * Matrix.fromCol(pnt.toArray :+ 1.0)
    Pnt(transformed(0, 0), transformed(1, 0))
  }

  override def transform(that: Transform): Transform = {
    // by transforming this transformation, we are specifying that the resulting transformation should be
    // equivalent to first applying this transformation, and then applying that transformation
    // transformable.transform(this.transform(that)) is equivalent to transformable.transform(this).transform(that)
    this.andThen(that)
  }

}

object Transform {

  val identity: Transform = Transform(Matrix.identity(3))

  // translation: | 1 0 dx |   | x |   | x + dx |
  //              | 0 1 dy | * | y | = | y + dy |
  //              | 0 0  1 |   | 1 |   |   1    |
  def translate(v: Vec): Transform = {
    Transform(Matrix.identity(2).
      augmentCols(Matrix.fromCol(v.toArray)).
      augmentRows(Matrix.fromRow(Array(0.0, 0.0, 1.0)))
    )
  }

  /*****************************************************************************************
   * you could call these convenience methods, but there are performance concerns          *
   * they produce a single transformation which can be applied once to each of many points *
   * rather that transforming each point many times                                        *
   *****************************************************************************************/
  def scale(scaleXY: Double, center: Pnt): Transform = {
    this.scale(scaleXY, scaleXY, center)
  }

  def scale(scaleX: Double, scaleY: Double, center: Pnt): Transform = {
    translate(-center.v).
      andThen(scale(scaleX, scaleY)).
      andThen(translate(center.v))
  }

  def pointReflect(center: Pnt): Transform = {
    translate(-center.v).andThen(originReflect).andThen(translate(center.v))
  }

  def linearReflect(line: Line): Transform = {
    translate(-line.intercept.v).
      andThen(linearReflect(line.dir)).
      andThen(translate(line.intercept.v))
  }

  def rotate(degrees: Double, center: Pnt): Transform = {
    rotateRadians(degrees * math.Pi / 180.0, center)
  }

  def rotateRadians(radians: Double, center: Pnt): Transform = {
    translate(-center.v).andThen(rotateRadians(radians)).andThen(translate(center.v))
  }


  /********************************************************
   * transformations centered at origin                   *
   * these are the ones that manipulate matrices directly *
   ********************************************************/
  def scale(scale: Double): Transform = {
    this.scale(scale, scale)
  }

  // scale: | sx  0 0 |   | x |   | sx*x |
  //        |  0 sy 0 | * | y | = | sy*y |
  //        |  0  0 1 |   | 1 |   |   1  |
  def scale(scaleX: Double, scaleY: Double) = {
    Transform(Matrix.fromRows(IndexedSeq(
      IndexedSeq(scaleX,    0.0, 0.0),
      IndexedSeq(   0.0, scaleY, 0.0),
      IndexedSeq(   0.0,    0.0, 1.0)
    )))
  }


  // point reflection: | -1  0  0 |   | x |   | -x |
  //                   |  0 -1  0 | * | y | = | -y |
  //                   |  0  0  1 |   | 1 |   |  1 |
  def originReflect: Transform = { // reflects through origin
    scale(-1)
  }


  // line (through origin) reflection:
  // rx2 = rx*rx
  // ry2 = ry*ry
  // rxy = rx*ry
  // delta = rx2 + ry2
  //   | (rx2 - ry2) / delta  2 * rxy / delta      0 |   | x |   | x * (rx2 - ry2) / delta + y * 2 * rxy / delta |
  //   | 2 * rxy / delta      (ry2 - rx2) / delta  0 | * | y | = | y * (ry2 - rx2) / delta + x * 2 * rxy / delta |
  //   |           0                  0            1 |   | 1 |   |                      1                        |
  def linearReflect(dir: Vec): Transform = { // reflects across line through origin with dir
    val rx2 = dir.x * dir.x
    val ry2 = dir.y * dir.y
    val rxy = dir.x * dir.y
    val delta = rx2 + ry2
    Transform(Matrix(IndexedSeq(
      IndexedSeq((rx2 - ry2) / delta, 2 * rxy / delta    , 0.0),
      IndexedSeq(2 * rxy / delta    , (ry2 - rx2) / delta, 0.0),
      IndexedSeq(                0.0,                 0.0, 1.0)
    )))
  }


  def rotate(degrees: Double): Transform = {
    rotateRadians(degrees * math.Pi / 180.0)
  }

  // rotation: | cos(t) -sin(t) 0 |   | x |   | cos(t)*x - sin(t)*y |
  //           | sin(t)  cos(t) 0 | * | y | = | sin(t)*x + cos(t)*y |
  //           |   0       0    1 |   | 1 |   |          1          |
  def rotateRadians(radians: Double): Transform = {
    Transform(Matrix.fromRows(IndexedSeq(
      IndexedSeq(math.cos(radians), -math.sin(radians), 0.0),
      IndexedSeq(math.sin(radians), math.cos(radians) , 0.0),
      IndexedSeq(              0.0,                0.0, 1.0)
    )))
  }

}
