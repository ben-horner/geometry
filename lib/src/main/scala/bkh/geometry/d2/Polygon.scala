package bkh.geometry.d2

case class Polygon(points: Seq[Pnt]) extends Transformable[Polygon] {

//  def union(that: Polygon): Polygon = {
//    val poly1 = this.toPolygon2D
//    val poly2 = that.toPolygon2D
//    Polygon.from(Polygons2D.union(poly1, poly2))
//  }
//
//  def intersection(that: Polygon): Polygon = {
//    val poly1 = this.toPolygon2D
//    val poly2 = that.toPolygon2D
//    Polygon.from(Polygons2D.intersection(poly1, poly2))
//  }
//
//  def difference(that: Polygon): Polygon = {
//    val poly1 = this.toPolygon2D
//    val poly2 = that.toPolygon2D
//    Polygon.from(Polygons2D.difference(poly1, poly2))
//  }

  // convex hull

  override def transform(transformation: Transform): Polygon = {
    Polygon(points.map(transformation(_)))
  }

}
