package bkh.geometry.d3

import bkh.geometry.d3.Vec.{X, Y, Z, ZERO}
import org.scalatest.funspec.AnyFunSpec

class VecSpec extends AnyFunSpec {

  describe("a.radiansFrom(b)") {
    it("should return such that a.rotateRadians(a.radiansFrom(b), a.cross(b)) == b") {
      val axes = Set(ZERO, X, Y, -X, Z, -X, -Y, -Z).toSeq
      val vecs = axes.combinations(3).map { case Seq(a, b, c) =>
        a + b + c
      }.toSet.
        filter(v => (v - ZERO).length > 0.0001).
        toSeq

      vecs.combinations(2).
        filter { case Seq(a, b) => a != -b }.
        foreach { case Seq(a, b) =>
          val angle = a.angleFrom(b)
          val commonPerp = a.cross(b)
          val result = a.rotate(angle, commonPerp)
          assert(result.angleFrom(b).abs < 0.00000001, "blah")
        }
    }
  }

}
