package bkh.geometry.d3

import org.scalatest.funspec.AnyFunSpec

import scala.util.Random

class TransformSpec extends AnyFunSpec {

  describe("a Vec Vec rotation") {
    it("should work") {
      val rng = new Random(42)
      for (i <- 0 until 100) {
        val rv1 = Vec(rng.nextDouble(), rng.nextDouble(), rng.nextDouble())
        val rv2 = Vec(rng.nextDouble(), rng.nextDouble(), rng.nextDouble())
        val rotated = rv1.rotate(rv1, rv2)
        assert(rv2.angleFrom(rotated) < 0.0000000001, s"result of rotating $rv1 to $rv2 should differ by angle of 0.0, was ${rv2.angleFrom(rotated)}")
      }
    }
    it("should work from all orientations") {
      val pnt = Pnt(0, 0, 10)
      assert(pnt.rotate(Vec.X,  Vec.Y).dist(Pnt(  0, 0,  10)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.X,  Vec.Z).dist(Pnt(-10, 0,   0)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.X, -Vec.Y).dist(Pnt(  0, 0,  10)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.X, -Vec.Z).dist(Pnt( 10, 0,   0)) < 0.0000000001, "msg")

      assert(pnt.rotate(Vec.Y,  Vec.X).dist(Pnt(  0,  0, 10)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Y,  Vec.Z).dist(Pnt(  0,-10,  0)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Y, -Vec.X).dist(Pnt(  0,  0, 10)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Y, -Vec.Z).dist(Pnt(  0, 10,  0)) < 0.0000000001, "msg")

      assert(pnt.rotate(Vec.Z,  Vec.X).dist(Pnt( 10,  0, 0)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Z,  Vec.Y).dist(Pnt(  0, 10, 0)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Z, -Vec.X).dist(Pnt(-10,  0, 0)) < 0.0000000001, "msg")
      assert(pnt.rotate(Vec.Z, -Vec.Y).dist(Pnt(  0,-10, 0)) < 0.0000000001, "msg")
    }
  }

  describe("a series of Transforms"){
    it("should combine to n equivalent single transform"){
      val pnt = Pnt(1, 1, 1)
      println(s"transforming point: $pnt")

      val directStep1 = pnt.translate(-pnt.v*1.5)
      val directStep2 = directStep1.rotate(Vec.Z, Vec.X, 180)
      val directStep3 = directStep2.rotate(Vec.X, Vec.Y, 30)
      val directStep4 = directStep3.translate(pnt.v*1.5)

      // component transformations
      val translateOnce = Transform.translate(-pnt.v*1.5)
      val rotateUpsideDown = Transform.rotate(Vec.Z, Vec.X, 180)
      val rotateHoriz = Transform.rotate(Vec.X, Vec.Y, 30)
      val translateAgain = Transform.translate(pnt.v*1.5)

      // transforming through components transformations, running total
      val matrixStep1 = pnt.transform(translateOnce)
      val matrixStep2 = matrixStep1.transform(rotateUpsideDown)
      val matrixStep3 = matrixStep2.transform(rotateHoriz)
      val matrixStep4 = matrixStep3.transform(translateAgain)

      // combining components step by step
      val combined1 = translateOnce
      val combined2 = translateOnce.andThen(rotateUpsideDown)
      val combined3 = translateOnce.andThen(rotateUpsideDown).andThen(rotateHoriz)
      val combined4 = translateOnce.andThen(rotateUpsideDown).andThen(rotateHoriz).andThen(translateAgain)

      // transforming by combined components at each step
      val combinedStep1 = pnt.transform(combined1)
      val combinedStep2 = pnt.transform(combined2)
      val combinedStep3 = pnt.transform(combined3)
      val combinedStep4 = pnt.transform(combined4)

      val combinedAtOnce = Transform.identity.
        andThen(translateOnce).
        andThen(rotateUpsideDown).
        andThen(rotateHoriz).
        andThen(translateAgain)
      assert(combinedAtOnce == combined4)

      val atOnce = pnt.transform(combinedAtOnce)

      // ensure direct transformations are equivalent to matrix versions of the operations step by step
      assert(matrixStep1.dist(directStep1) < 0.00000001, s"after step1, matrix and direct off by ${matrixStep1.dist(directStep1)}, $matrixStep1, and $directStep1")
      assert(matrixStep2.dist(directStep2) < 0.00000001, s"after step2, matrix and direct off by ${matrixStep2.dist(directStep2)}, $matrixStep2, and $directStep2")
      assert(matrixStep3.dist(directStep3) < 0.00000001, s"after step3, matrix and direct off by ${matrixStep3.dist(directStep3)}, $matrixStep3, and $directStep3")
      assert(matrixStep4.dist(directStep4) < 0.00000001, s"after step4, matrix and direct off by ${matrixStep4.dist(directStep4)}, $matrixStep4, and $directStep4")
      // ensure combined matrix transformations are equivalent to combined matrix operations step by step
      assert(matrixStep1.dist(combinedStep1) < 0.00000001, s"after step1, matrix and combined off by ${matrixStep1.dist(combinedStep1)}, $matrixStep1, and $combinedStep1")
      assert(matrixStep2.dist(combinedStep2) < 0.00000001, s"after step2, matrix and combined off by ${matrixStep2.dist(combinedStep2)}, $matrixStep2, and $combinedStep2")
      assert(matrixStep3.dist(combinedStep3) < 0.00000001, s"after step3, matrix and combined off by ${matrixStep3.dist(combinedStep3)}, $matrixStep3, and $combinedStep3")
      assert(matrixStep4.dist(combinedStep4) < 0.00000001, s"after step4, matrix and combined off by ${matrixStep4.dist(combinedStep4)}, $matrixStep4, and $combinedStep4")

      assert(matrixStep4.dist(atOnce)        < 0.00000001, s"transformed points didn't match, off by ${matrixStep4.dist(atOnce)}")
    }
  }

}
