package bkh.geometry.d2

import org.scalatest.funspec.AnyFunSpec

class SegmentSpec extends AnyFunSpec {

  describe("A Segment instance"){
    it("should produce a correct perpendicular bisector") {
      val p11 = Pnt(0, -0.5)
      val p12 = Pnt(1.5, 1.5)
      val s1 = Segment(p11, p12)
      println(s1.toLine.dir)
      val s1pb = s1.perpBisector
      assert(math.abs(s1.toLine.dir.angleFrom(s1pb.dir)) === 90.0, s"angle between segment dir (${s1.toLine.dir}) and bisector dir (${s1pb.dir}) was not 90 degrees")

      val p21 = Pnt(1.5, 1.5)
      val p22 = Pnt(0, 2.5)
      val s2 = Segment(p21, p22)
      val s2pb = s2.perpBisector
      assert(math.abs(s2.toLine.dir.angleFrom(s2pb.dir)) === 90.0, s"angle between segment dir (${s2.toLine.dir}) and bisector dir (${s2pb.dir}) was not 90 degrees")
    }
  }



}
