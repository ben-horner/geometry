package bkh.geometry.d2

import org.scalatest.funspec.AnyFunSpec

class VecSpec extends AnyFunSpec {

  describe("A Vec instance") {
    it("should be translatable") {
      val v = Vec(1, 1)
      val translated = v.translate(Vec(1, 1))
      val expected = Vec(2, 2)
      assert(translated === expected)
    }
  }

}
